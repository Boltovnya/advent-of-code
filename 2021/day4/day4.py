import numpy as np
from itertools import product

def load_file(file):
    with open(file, "r") as f:
        draws = f.readline()
        boards = f.read()[1:-1]
    return (draws, boards)

make_draws = lambda x: np.fromstring(x, dtype=int, sep=',')
make_boards = lambda x: [np.mat(b.replace("\n", ";")) for b in x.split("\n\n")]

def part1(draws, boards):
    boards_masked = list(map(np.ma.masked_array, boards))
    for d in draws:
        for b in boards_masked:
            b.mask |= b.data == d
            if np.any(b.mask.sum(0) == 5) or np.any(b.mask.sum(1) == 5):
                return (d, np.sum(b))

def part2(draws, boards):
    board_ids = set()
    winner = 0
    boards_masked = list(map(np.ma.masked_array, boards))
    for d, (i, b) in product(draws, enumerate(boards_masked)):
        b.mask |= b.data == d
        if np.any(b.mask.sum(0) == 5) or np.any(b.mask.sum(1) == 5):
            if i not in board_ids and len(board_ids) == len(boards) -1:
                winner = b.sum()*d
            board_ids.add(i)
    return (winner)

    

def main():
    d,b = load_file("input.txt")
    draws = make_draws(d)
    boards = make_boards(b)
    p1a,p1b = part1(draws, boards)
    p2 = part2(draws, boards)
    print(f"Part 1: {p1a*p1b}")
    print(f"Part 2: {p2}")

if __name__ == "__main__":
    main()

