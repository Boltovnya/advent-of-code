import numpy as np


def load_data(name):
    with open(name) as f:
        return f.read().splitlines()
    

def part1(arr):
    simple_nums = dict.fromkeys([2,4,3,7], 0)
    arr_split = [tuple(a.split(" | ")) for a in arr]
    for i in arr_split:
        for n in simple_nums:
            simple_nums[n] += [len(d) for d in i[1].split(" ")].count(n)
    return sum(simple_nums.values())
    
def part2(arr):
    total = 0
    for line in arr:
        num_string = ""
        cipher = {k:np.ma.masked_array(['a','b','c','d','e','f','g']) for k in range(0,10)} # Masked arrays make the process of eliminating unused characters easy.
        s = line.split(" | ")[0].split()
        for j in s:
            match len(j): # We know that Lens 2,3,4,7 == Chars 1,4,7,8 - so work through the list of signals and mask off the Chars that appear in signals of those lengths.
                case 2:
                    for k in j:
                        cipher[1].mask |= cipher[1].data == k 
                case 4:
                    for k in j:
                        cipher[4].mask |= cipher[4].data == k
                case 7:
                    for k in j:
                        cipher[8].mask |= cipher[8].data == k
                case 3:
                    for k in j:
                        cipher[7].mask |= cipher[7].data == k
                case _:
                    pass
        for j in filter(lambda x: len(x) not in [2,4,7,3], s):
            t = np.ma.masked_array(['a','b','c','d','e','f','g'])
            for k in j: # Mask off the current signal chars
                t.mask |= t.data == k 
            match len(j), len(list(filter(lambda x: x,t.mask&cipher[1].mask))), len(list(filter(lambda x: x,t.mask&cipher[4].mask))), len(list(filter(lambda x: x,t.mask&cipher[7].mask))):
                # match a,b,c,d
                # a = Length of signal
                # b = Length of signal after masking off chars from 1
                # c = Length of signal after masking off chars from 4
                # d = Length of signal after masking off chars from 7
                # Actual cases worked out by using pixel art. Image in repo.
                case 5, 1, 2, 2:
                    cipher[2].mask = t.mask
                case 5, 2, 3, 3:
                    cipher[3].mask = t.mask
                case 5, 1, 3, 2:
                    cipher[5].mask = t.mask
                case 6, 1, 3, 2:
                    cipher[6].mask = t.mask
                case 6, 2, 4, 3:
                    cipher[9].mask = t.mask
                case 6, 2, 3, 3:
                    cipher[0].mask = t.mask
        full = {k:cipher[k].data[cipher[k].mask] for k in cipher} # Flatten masked array to only masked values
        final = {''.join(v):k for (k,v) in full.items()} # Reverse dict, turn char string into key, and num into value
        for o in line.split(" | ")[1].split():
            ot = ''.join(sorted(o))
            num_string += str(final[ot]) 
        total += int(num_string)
    return total

def main():
    data = load_data("2021/day8/input.txt")
    p1 = part1(data)
    p2 = part2(data)
    print(f"Part 1: {p1}")
    print(f"Part 2: {p2}")
if __name__ == "__main__":
    main()