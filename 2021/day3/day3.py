import numpy as np


def load_file(name):
    return open(name, "r").readlines()


def int_to_list(cell):
    """
    [11011010] -> [1,1,0,1,1,0,1,0]
    """
    return [int(d) for d in str(cell).strip()]


def rotate_axes(arr):
    """
    [[1,1,0,1,1,0,1,0], [0,0,1,0,1,1,0,1]] -> [[1,0],[1,0],[0,1]...]
    """
    return np.transpose(arr)


def gamma(arr):
    for i in arr:
        ones = (i == 1).sum()
        zeros = (i == 0).sum()
        if ones > zeros:
            yield 1
        if zeros > ones:
            yield 0
        if zeros == ones:
            yield 1


def epsilon(arr):
    for i in arr:
        ones = (i == 1).sum()
        zeros = (i == 0).sum()
        if ones < zeros:
            yield 1
        if zeros < ones:
            yield 0
        if zeros == ones:
            yield 0


def part1(arr):
    workable = list(map(int_to_list, arr))
    rotated = rotate_axes(workable)
    g = list(gamma(rotated))
    e = list(epsilon(rotated))
    g_string = "".join([str(i) for i in g])
    e_string = "".join([str(i) for i in e])
    g_int = int(g_string, 2)
    e_int = int(e_string, 2)
    return g_int*e_int


def o2(arr):
    workable = np.array(list(map(int_to_list, arr)))

    def inner(arr, follower):
        if follower == len(arr[0]):
            return arr
        else:
            rotated = rotate_axes(arr)
            g = list(gamma(rotated))[follower]
            res = np.delete(arr, np.where(arr[:, follower] != g), 0)
            return inner(res, follower+1)
    x = inner(workable, 0)
    return x


def co2(arr):
    workable = np.array(list(map(int_to_list, arr)))

    def inner(arr, follower):
        if follower == len(arr[0]) or len(arr) == 1:
            return arr
        else:
            rotated = rotate_axes(arr)
            g = list(epsilon(rotated))[follower]
            res = np.delete(arr, np.where(arr[:, follower] != g), 0)
            return inner(res, follower+1)
    x = inner(workable, 0)
    return x


def part2(arr):
    o = o2(arr).flatten().tolist()
    c = co2(arr).flatten().tolist()
    o_string = "".join([str(i) for i in o])
    c_string = "".join([str(i) for i in c])
    o_int = int(o_string, 2)
    c_int = int(c_string, 2)
    return o_int * c_int


def main():
    arr = load_file("input.txt")
    p1 = part1(arr)
    p2 = part2(arr)
    print(f"Part 1: {p1}")
    print(f"Part 2: {p2}")


if __name__ == "__main__":
    main()
