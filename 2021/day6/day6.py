from typing import Counter
import numpy as np

def load_data(name):
    return np.fromfile(name, dtype=np.int_, sep=",")

def feesh(fish, n):
    if n != 0:
        temp = dict.fromkeys(range(0,9), 0)
        for f in fish:
            if fish[f] == 0:
                continue    
            elif f == 0:
                temp[6] += fish[f]
                temp[8] += fish[f]
                temp[f] = 0
            else:
                temp[f-1] += fish[f]                
        return feesh(temp, n-1)
    else:
        return fish

def main():
    data = load_data("input.txt")
    sushi = {i:len(data[data == i]) for i in data}
    p1 = sum(feesh(sushi, 80).values())
    p2 = sum(feesh(sushi, 256).values())
    p3 = sum(feesh(sushi, 50000).values())
    print(f"Part 1: {p1}")
    print(f"Part 2: {p2}")
    print(f"Part 3: {p3}")

if __name__ == "__main__":
    main()