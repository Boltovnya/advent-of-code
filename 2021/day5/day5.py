"""
Helper = lambda x: x.split(",")
Input  = "12,5 -> 15,2"
Step 1 = Input.split(" -> ") => ["12,5", "15,2"]
Step 2 = [(int(a), int(b)) for a,b in map(Helper, Step 1)] = [(12,5), (15,2)]
"""

import numpy as np

split_helper = lambda x: x.split(",")
filter_helper = lambda f: f is not None
process_data = lambda f: [(int(a), int(b)) for a,b in map(split_helper, f.split(" -> "))]
build_grid = lambda n: np.zeros((n,n))



def check_straight(line):
    match line:
        case [(x1, _), (x2, _)] if x1 == x2:
            return line
        case [(_, y1), (_, y2)] if y1 == y2:
            return line         

def load_data(name):
    with open(name) as f:
        lines = f.read().splitlines()
    return list(map(process_data,lines))

def draw_lines(data, g):
    total = 0
    for line in data:
        print(line)
        match line:
            case [(x1, y1), (x2, y2)] if x1 == x2:
                if y2 > y1:   ry = range(y1, y2 + 1)
                elif y1 > y2: ry = range(y1, y2 - 1, -1)
                else:         ry = [y1]

                for idx in ry:
                    g[idx][x1] += 1 
                    total += 1
                continue

            case [(x1, y1), (x2, y2)] if y1 == y2:
                if x2 > x1:   rx = range(x1, x2 + 1)
                elif x1 > x2: rx = range(x1, x2 - 1, -1)
                else:         rx = [x1]

                for idx in rx:
                    g[y1][idx] += 1
                    total += 1
                continue
            case [(x1, y1), (x2, y2)] if x1 == x2 == y1 == y2:
                g[y1][x1] += 1
                total += 1
                continue
            case [(x1, y1), (x2, y2)]:
                m = ((y2-y1)/(x2-x1))
                if abs(m) != 1:
                    continue
                if y2 > y1:   ry = range(y1, y2 + 1)
                elif y1 > y2: ry = range(y1, y2 - 1, -1)
                else:         ry = [y1]

                if x2 > x1:   rx = range(x1, x2 + 1)
                elif x1 > x2: rx = range(x1, x2 - 1, -1)
                else:         rx = [x1]

                test = zip(rx, ry)

                for idx, idy in test:
                    g[idy][idx] += 1
                    total += 1
    return g
                


def part1(data):
    d = np.asarray(data)
    g = build_grid(d.max()+1)
    straight_lines = list(filter(filter_helper, map(check_straight, data)))
    res = draw_lines(straight_lines, g)  
    return len(res[res > 1])


def part2(data):
    d = np.asarray(data)
    g = build_grid(d.max()+1)
    res = draw_lines(data, g)
    with open("output_test.txt", "w") as f:
        for i in res:
            f.write(str(i.tolist()))  
    return len(res[res > 1])


def main(): 
    data = load_data("input.txt")
    p1 = part1(data)
    p2 = part2(data)
    print(f"Part 1: {p1}")
    print(f"Part 2: {p2}")

if __name__ == "__main__":
    main()