import numpy as np
import numpy.lib.stride_tricks as strides

def gen_slides(arr, size):
    return strides.sliding_window_view(arr, size)

def load_file(name):
    return np.fromfile(name, sep="\n")

def part1(arr):
    slides = gen_slides(arr, 2)
    increased = 0
    for i in slides:
        if i[1] > i[0]:
            increased +=1 
    return increased

def part2(arr):
    slides = gen_slides(arr, 3)
    sums = list(map(np.sum, slides))
    sums_arr = np.array(sums)
    comparisons = part1(sums_arr)
    return comparisons

def main():
    arr = load_file("input.txt")
    p1 = part1(arr)
    p2 = part2(arr)
    print(f"There are {p1} measurements larger than the previous")
    print(f"There are {p2} sliding measurements larger than the previous")


if __name__ == "__main__":
    main()