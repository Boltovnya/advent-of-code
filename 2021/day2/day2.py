def split_spaces(cell):
    return cell.split(" ")

def gen_instruction(arr):
    return list(map(split_spaces, arr))

def load_file(name):
    return open(name, "r").readlines()

def part1(code):
    forward = 0
    depth = 0
    for i in code:
        match i:
            case ["forward", x]:
                forward += int(x)
            case ["down", x]:
                depth += int(x)
            case ["up", x]:
                depth -= int(x)
            case _:
                print("wat")
    return forward * depth

def part2(code):
    forward = 0
    depth = 0
    aim = 0
    for i in code:
        match i:
            case ["forward", x]:
                forward += int(x)
                depth += aim * int(x)
                print(f"Got \"Forward {x}\": Forward = {forward} & Depth = {depth}")
            case ["down", x]:
                aim += int(x)
                print(f"Got \"Down {x}\": Depth = {depth}")
            case ["up", x]:
                aim -= int(x)
                print(f"Got \"Up {x}\": Depth = {depth}")

    return forward * depth

def main():
    arr = load_file("./input.txt")
    compiled_r = gen_instruction(arr)
    p1 = part1(compiled_r)
    p2 = part2(compiled_r)
    print(f"Part 1: {p1}")
    print(f"Part 2: {p2}")



    return 0

if __name__ == "__main__":
    main()