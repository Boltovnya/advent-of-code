import numpy as np
from scipy.optimize import minimize_scalar

coeff = lambda n: ((n**2)+n)/2

def load_data(name):
    return np.fromfile(name, sep=",", dtype=int)

def part1(arr):
    return minimize_scalar(lambda x: np.sum(np.abs(x - arr)))

def part2(arr):
    return minimize_scalar(lambda x: np.sum((coeff(np.abs(round(x) - arr)))))

def main():
    data = load_data("input.txt")
    p1 = part1(data)
    p2 = part2(data)

    print(f"Part 1: {round(p1.fun)}")
    print(f"Part 2: {round(p2.fun)}")

if __name__ == "__main__":
    main()